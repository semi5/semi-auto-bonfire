export function setup(ctx) {
	// debug
	const debugLog = (...msg) => {
		mod.api.SEMI.log(`${id} v10045`, ...msg);
	};

	// variables
	const id = "semi-auto-bonfire";
	const name = "SEMI Auto Bonfire";

	let config = {
		enabled: false
	};

	// config
	const storeConfig = () => {
		ctx.characterStorage.setItem('config', config);
	}

	const loadConfig = () => {
		const storedConfig = ctx.characterStorage.getItem('config');
		if (!storedConfig) {
			return;
		}

		config = { ...config, ...storedConfig };
	}

	// ui
	const injectUI = () => {
		const node = document.getElementById('firemaking-bonfire-menu').children[0].children[0].children[0];

		const check = createElement('div', { 'className': 'col-12' });
		check.innerHTML = `<div class="block block-rounded-double bg-combat-inner-dark p-4 justify-horizontal-center">
			<div class="custom-control custom-switch custom-control-lg">
				<input class="custom-control-input" type="checkbox" name="${id}-enable-check" id="${id}-enable-check">
				<label class="font-weight-normal ml-2 custom-control-label" for="${id}-enable-check">Enable ${name}</label>
			</div>
		</div>`;

		node.append(check);

		$(`#${id}-enable-check`).on('change', function (e) {
			toggleEnabledStatus();
		});

		$(`#${id}-enable-check`).prop('checked', config.enabled);
	};

	const toggleEnabledStatus = () => {
		config.enabled = !config.enabled;
		storeConfig();
	};

	// script
	const onBeforeAction = () => {
		if (!config.enabled)
			return;

		if (game.firemaking.isLogSelected && !game.firemaking.isBonfireActive) {
			const recipe = game.firemaking.selectedRecipe;
			const isAbyssal = recipe.hasAbyssalBonfire;

			const bonfireCosts = new Costs(game);
			if (!game.modifiers.freeBonfires || isAbyssal) {
				bonfireCosts.addItem(recipe.log, 10);
			}
			if (bonfireCosts.checkIfOwned()) {
				game.firemaking.lightBonfire();
			}
		}
	};

	// hooks + game patches
	ctx.onCharacterLoaded(ctx => {
		loadConfig();

		ctx.patch(Firemaking, 'action').before(() => {
			onBeforeAction();
		});
	});

	ctx.onInterfaceReady(() => {
		injectUI();
	});
}